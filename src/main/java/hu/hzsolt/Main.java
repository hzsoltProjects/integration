package hu.hzsolt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.flexionmobile.codingchallenge.integration.IntegrationTestRunner;
import hu.hzsolt.purchase.*;

/**
 * @author zshorvath
 * created on 06/11/2022
 */
public class Main {
    public static void main(String[] args) {
        PurchaseService purchaseService = new PurchaseServiceFactory(new ObjectMapper()).createPurchaseService();
        new IntegrationTestRunner().runTests(purchaseService);
    }
}