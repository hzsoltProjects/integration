package hu.hzsolt.error;

/**
 * @author zshorvath
 * created on 06/11/2022
 */
public class FlexionMobileRequestException extends RuntimeException {

    public FlexionMobileRequestException(String message) {
        super(message);
    }
}
