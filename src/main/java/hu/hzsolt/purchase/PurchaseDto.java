package hu.hzsolt.purchase;

import com.flexionmobile.codingchallenge.integration.Purchase;

/**
 * @author zshorvath
 * created on 06/11/2022
 */
public class PurchaseDto implements Purchase {

    private String id;
    private boolean consumed;
    private String itemId;

    public PurchaseDto() {
    }

    public PurchaseDto(String id, boolean consumed, String itemId) {
        this.id = id;
        this.consumed = consumed;
        this.itemId = itemId;
    }

    public PurchaseDto setId(String id) {
        this.id = id;
        return this;
    }

    public PurchaseDto setConsumed(boolean consumed) {
        this.consumed = consumed;
        return this;
    }

    public PurchaseDto setItemId(String itemId) {
        this.itemId = itemId;
        return this;
    }
    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public boolean getConsumed() {
        return this.consumed;
    }

    @Override
    public String getItemId() {
        return this.itemId;
    }
}
