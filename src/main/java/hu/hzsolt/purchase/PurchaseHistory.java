package hu.hzsolt.purchase;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.flexionmobile.codingchallenge.integration.Purchase;

import java.util.List;

/**
 * @author zshorvath
 * created on 06/11/2022
 */
public class PurchaseHistory {

    @JsonDeserialize(contentAs = PurchaseDto.class)
    private List<Purchase> purchases;

    public List<Purchase> getPurchases() {
        return purchases;
    }

    public PurchaseHistory setPurchases(List<Purchase> purchases) {
        this.purchases = purchases;
        return this;
    }
}
