package hu.hzsolt.purchase;

import com.flexionmobile.codingchallenge.integration.Integration;
import com.flexionmobile.codingchallenge.integration.Purchase;
import hu.hzsolt.purchase.request.PurchaseConsumeRequestSender;
import hu.hzsolt.purchase.request.PurchaseRequestSender;
import hu.hzsolt.purchase.request.PurchaseRetrieveRequestSender;

import java.util.List;

/**
 * @author zshorvath
 * created on 06/11/2022
 */
public class PurchaseService implements Integration {

    private final PurchaseRequestSender purchaseRequestSender;
    private final PurchaseRetrieveRequestSender purchaseRetrieveRequestSender;
    private final PurchaseConsumeRequestSender purchaseConsumeRequestSender;

    public PurchaseService(
            PurchaseRequestSender purchaseRequestSender, PurchaseRetrieveRequestSender purchaseRetrieveRequestSender,
            PurchaseConsumeRequestSender purchaseConsumeRequestSender
    ) {
        this.purchaseRequestSender = purchaseRequestSender;
        this.purchaseRetrieveRequestSender = purchaseRetrieveRequestSender;
        this.purchaseConsumeRequestSender = purchaseConsumeRequestSender;
    }

    @Override
    public Purchase buy(String s) {
      return purchaseRequestSender.sendBuyItemRequest(s);
    }

    @Override
    public List<Purchase> getPurchases() {
        return purchaseRetrieveRequestSender.sendGetAllPurchasesRequest();
    }

    @Override
    public void consume(Purchase purchase) {
        purchaseConsumeRequestSender.sendConsumePurchaseRequest((PurchaseDto)purchase);
    }
}
