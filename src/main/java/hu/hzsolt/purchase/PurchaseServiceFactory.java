package hu.hzsolt.purchase;

import com.fasterxml.jackson.databind.ObjectMapper;
import hu.hzsolt.purchase.request.PurchaseConsumeRequestSender;
import hu.hzsolt.purchase.request.PurchaseRequestSender;
import hu.hzsolt.purchase.request.PurchaseRetrieveRequestSender;

/**
 * @author zshorvath
 * created on 06/11/2022
 */
public class PurchaseServiceFactory {

    private final ObjectMapper objectMapper;

    public PurchaseServiceFactory(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public PurchaseService createPurchaseService() {
        return new PurchaseService(
                new PurchaseRequestSender(objectMapper),
                new PurchaseRetrieveRequestSender(objectMapper),
                new PurchaseConsumeRequestSender()
        );
    }
}
