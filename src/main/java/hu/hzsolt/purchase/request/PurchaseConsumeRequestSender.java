package hu.hzsolt.purchase.request;

import hu.hzsolt.error.FlexionMobileRequestException;
import hu.hzsolt.purchase.PurchaseDto;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * @author zshorvath
 * created on 06/11/2022
 */
public class PurchaseConsumeRequestSender {

    private static final String REQUEST_URL =
            "https://recruitment-task.flexionmobile.com/javachallenge/rest/developer/zsoltHorvath/consume/";

    public void sendConsumePurchaseRequest(PurchaseDto purchase) {
        try {
            HttpResponse<String> response = HttpClient
                    .newHttpClient()
                    .send(createRequest(purchase.getId()), HttpResponse.BodyHandlers.ofString()
                    );
            if (response.statusCode() != 200) {
                throw new FlexionMobileRequestException(
                        "Unexpected response: '%s'".formatted(response.body())
                );
            }
            purchase.setConsumed(true);
        } catch (URISyntaxException | IOException | InterruptedException exception) {
            throw new RuntimeException("Error during sending request: '%s'".formatted(exception.getMessage()));
        }
    }

    private HttpRequest createRequest(String id) throws URISyntaxException {
        return HttpRequest.newBuilder()
                .uri(new URI(REQUEST_URL + id))
                .POST(HttpRequest.BodyPublishers.noBody())
                .build()
                ;
    }
}
