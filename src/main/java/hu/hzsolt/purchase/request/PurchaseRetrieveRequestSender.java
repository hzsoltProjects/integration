package hu.hzsolt.purchase.request;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.flexionmobile.codingchallenge.integration.Purchase;
import hu.hzsolt.error.FlexionMobileRequestException;
import hu.hzsolt.purchase.PurchaseHistory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

/**
 * @author zshorvath
 * created on 06/11/2022
 */
public class PurchaseRetrieveRequestSender {

    private static final String REQUEST_URL =
            "https://recruitment-task.flexionmobile.com/javachallenge/rest/developer/zsoltHorvath/all";
    private final ObjectMapper objectMapper;

    public PurchaseRetrieveRequestSender(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public List<Purchase> sendGetAllPurchasesRequest() {
        try {
            HttpResponse<String> response =
                    HttpClient.newHttpClient().send(createRequest(), HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != 200) {
                throw new FlexionMobileRequestException(
                        "Unexpected response: '%s'".formatted(response.body())
                );
            }
            PurchaseHistory purchaseHistory = objectMapper.readValue(response.body(), PurchaseHistory.class);
            return purchaseHistory.getPurchases();
        } catch (IOException | InterruptedException | URISyntaxException exception) {
            throw new RuntimeException("Error during sending request: '%s'".formatted(exception.getMessage()));
        }
    }

    private HttpRequest createRequest() throws URISyntaxException {
        return HttpRequest.newBuilder()
                .uri(new URI(REQUEST_URL))
                .GET()
                .build()
                ;
    }
}
